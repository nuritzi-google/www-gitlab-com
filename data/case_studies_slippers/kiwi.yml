title: Kiwi
file_name: kiwi
canonical_path: /customers/kiwi/
cover_image: /images/blogimages/kiwi-cover.jpg
cover_title: |
  How Kiwi.com transformed its workflow with GitLab and Docker
cover_description: |
  GitLab provides Kiwi.com with CI stability, ease of integration for containers, and source control management
twitter_image: /images/blogimages/kiwi-cover.jpg
customer_logo: /images/case_study_logos/kiwi.png
customer_logo_css_class: brand-logo-tall
customer_industry: Technology
customer_location: Brno, Czech Republic
customer_solution: GitLab Self-Managed Ultimate
customer_employees: 2000+
customer_overview: |
  GitLab’s ease of integration allows Kiwi.com to use containers, manage collaboration, and boost deployment.
customer_challenge: |
  Kiwi.com lacked a single source of truth and a way to integrate CI/CD, causing slow deployments and more work for DevOps teams.

key_benefits: |-

  
    Increased operational efficiency

  
    Provides a single source of truth

  
    Integrates with Docker, Kubernetes, Terraform

  
    Improved quality of code
customer_stats:
  - stat: 10,000  
    label: Average builds per month
  - stat: 10     
    label: Minute deploys
  - stat: 100%     
    label: Adoption rate
customer_study_content:
  - title: the customer
    subtitle: Virtual Global Supercarrier
    content: >-
    
  
        Kiwi.com is an online travel-tech company that offers travel itineraries to customers globally. Its proprietary algorithm - Virtual Interlining - allows users to combine flights and ground transportation from more than 800 carriers, including many that do not normally participate in offerings. All the connections are covered by the leader in the travel industry, Kiwi.com’s Guarantee, which protects the customers from missed connections caused by delay, schedule change, or cancellation.
    
  - title: the challenge
    subtitle: Inadequate delivery system, CI, and source control
    content: >-
    
  
        After [Kiwi.com](https://www.kiwi.com/us/) evolved from the startup phase, they started a plan to unify their systems. Firstly, there was no consistency between how different teams deployed their services, mainly because Docker wasn’t an essential part of the workflow. Secondly, the historical information of how that software was developed was in disparate silos and not documented properly.    
    
  
        Kiwi.com didn’t have continuous integration or delivery systems in place. “The release process was long and even rolling back wasn't an easy task. Everything was done manually,” Alex Viscreanu, Technical Team Lead at Kiwi.com said. “There was no automation at all.”
   
  - title: the solution
    subtitle: Choosing a unified platform
    content: >-
    
  
        The development team was initially working with different software platforms, but found that running shell commands wasn’t a good experience for them. Neither tool had proper support for containers, which was a priority. They tested GitLab and discovered that it offered the most well-integrated platform. “When we tried GitLab and we saw how well CI/CD was integrated with everything and that we could use CI/CD for automatically merging code, trusting our checks, and not worrying about all the things, it was like a no brainer,” Viscreanu said.

  - title: the results
    subtitle: Integrating with Docker, Kubernetes, and Terraform for CI success
    content: >-
    
  
        The first big issue that GitLab solved was the easy [integration with Docker](/releases/2019/07/31/docker-in-docker-with-docker-19-dot-03/), allowing the team to leverage containers as the main way to package software. Previously, they hadn’t worked in containers, but with GitLab CI, they started using them everywhere. “Once we had that in place and we had an easy way to verify the code and automate the deployment, everything was so well integrated that we could trigger actions directly from GitLab” Viscreanu said. “Right now we have automated deployment, automated dependency maintenance, automated dependency scanning, automated dependency license scanning, automated everything.”

        
        With GitLab’s automation in place, Kiwi.com now averages:

          * 4,000 commits per day
          * 1,500 deployments per month
          * 47,000 tests per month
          * 5,000 MRs per month 
    
  
        When containers became well established in the company, Kiwi.com went through different container platforms, most of which ended up limiting the scalability of their services. After dealing with those limitations, and creating workarounds to fit their use case, they didn’t want to invest more effort into products that weren't going to be developed or be relevant any longer. Developers were looking for a solution that would consistently manage their resources and enable them to build tooling around it. The main reason for this, according to [Viscreanu](https://youtu.be/W1biIw0uM-4), is “Sanity. There is no way that you can keep track of all these tools when you are a big company if you don’t automate them and unify things.”      
    
  
        GitLab’s integration with Kubernetes has made their work noticeably simpler, mainly because of how well integrated it is with the rest of the solution. Viscreanu and his team are also pushing a lot of infrastructure as code, using Terraform in GitLab CI. It allows them to deploy, create, modify, and lead infrastructure pieces without having to do it manually. “If it's not in CI and it's not in GitLab, then changes shouldn't be done,” Viscreanu added.
    
  
        Keeping everything in CI means having a record of any modifications and provides a single source of truth. “At the beginning I think that CI/CD was the main feature and I think that even right now it's still the most beloved feature for us. We’re using Git as the source of truth, which allows us to prevent having everything distributed and everything maintained by the teams themselves and then forgotten somehow,” Viscreanu said. 
    
  
        Prior to GitLab, the deployment process was slow because there was no certainty that the code that was pushed was working. At that point, the only way to check the code was to deploy and learn after the fact. Now, despite the increase in services, employees, projects, microservices, and overall expansion of the company, the deployment is succinct with CI. Reaffirming their confidence in GitLab “Right now with CI, even if we don't catch everything, we really trust it, at least for the critical paths. And with CD and approaches like rolling releases, even if the release has issues, we are able to react fast, before it affects all our customers,” Viscreanu said. 
      
      
    
  
        ## Learn more about Self-Managed Ultimate
    
  
        [Why source code management matters](/stages-devops-lifecycle/source-code-management/)
    
  
        [The benefits of GitLab CI](/stages-devops-lifecycle/continuous-integration/)
    
  
        [Choose a plan that suits your needs](/pricing/)
customer_study_quotes:
  - blockquote: I haven't seen any platform where it's as easy to use CI for automating everything as it is in GitLab.
    attribution: Alex Viscreanu 
    attribution_title: Technical Team Lead, Kiwi.com
